import cv2
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import CustomObjectScope
from tensorflow.keras.initializers import glorot_uniform

def adjust_gamma(image, gamma=0.25):
    invGamma = 1.0/gamma
    table = np.array([((i/255.0)**invGamma)*200 for i in range(0,256)]).astype('uint8')
    return cv2.LUT(image,table)


if __name__ == '__main__':
    vobj = cv2.VideoCapture('rtsp:admin:admin@172.16.100.100:554')
    rois = [
        (0, 0, 50, 100), 
        (60, 0, 160, 100), 
        (175, 0, 270, 100), 
        (285, 0, 380, 100), 
        (390, 0, 480, 100), 
        (490, 0, 580, 100), 
        (0,480,150,575), 
        (170,480,320,575),
        (340,480,490,575),
        (510,480,660,575)
    ]

    outputFile = 'prediction.txt'    

    counter = 30
    with CustomObjectScope({'GlorotUniform': glorot_uniform()}):
        model = load_model('new.h5')

        while True:
            status,frame = vobj.read()
            if not status:
                vobj = cv2.VideoCapture('rtsp:admin:admin@172.16.100.100:554')
                continue

            if counter%30 == 0:
                vagas = [cv2.resize((frame[roi[1]:roi[3],roi[0]:roi[2]]), (200,200))  for roi in rois]  
                prediction = model.predict(np.array(vagas))
                with open(outputFile, 'w') as f:
                    write_data = ';'.join([str(vaga) for vaga in prediction])
                    f.write(write_data)
            for n, r in enumerate(rois):
                cv2.rectangle(frame, (r[0], r[1]), (r[2], r[3]),(0, 0, 255) if prediction[n]==1 else (0, 255, 0), 3)

            counter += 1
            cv2.imshow('RealTime Image', frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break    
vobj.release()
cv2.destroyAllWindows()
