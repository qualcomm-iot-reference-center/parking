# -*- coding: utf-8 -*-
import dash
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import plotly
import plotly.graph_objs as go
from datetime import datetime, timedelta
import mysql.connector
from flask_mysqldb import MySQL
from Data import DataSmartParking

# Inicialize data
Data = DataSmartParking()

# CSS style
# external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

# Dash application
app = dash.Dash(external_stylesheets=[dbc.themes.LUX])
app.config["suppress_callback_exceptions"] = True

# Flask server
server = app.server


def get_busy_spot():
    busiest, less_busy = Data.busy_spot()
    return html.Div(
        [
            html.Div(
                children=[
                    html.Div(
                        [
                            html.P(""),
                            html.P("Busiest Spot  = {}".format(busiest)),
                            html.P("Less Busy Spot  = {}".format(less_busy)),
                        ]
                    )
                ],
                className="text",
            ),
        ]
    )


# Dashboard's layout
app.layout = html.Div(
    [
        html.Div(
            [html.H1("Smart Parking"), html.Img(src="/static/images/logo.png")],
            className="banner",
        ),
        html.Br(),
        # Divisoria criada para inserir o grafico e as tabs no mesmo bloco
        html.Div(
            [
                # Divisoria criada para posicionar o grafico
                dbc.Row(
                    [
                        dbc.Col(
                            html.Div(
                                [
                                    dcc.Graph(id="Cars-per-days"),
                                    dcc.Interval(id="graph-update", interval=300000),
                                ],
                            ),
                            width=6,
                        ),
                        # Divisoria criada para posicionar as tabs em relacao ao grafico
                        dbc.Col(
                            html.Div(
                                [
                                    dcc.Tabs(
                                        id="Tabs",
                                        value="About",
                                        parent_className="custom-tabs",
                                        className="custom-tabs-container",
                                        children=[
                                            dcc.Tab(
                                                label="About",
                                                value="About",
                                                className="custom-tab",
                                                selected_className="custom-tab--selected",
                                            ),
                                            dcc.Tab(
                                                label="Parking's Data",
                                                value="Data",
                                                className="custom-tab",
                                                selected_className="custom-tab--selected",
                                            ),
                                        ],
                                    ),
                                    html.Div(id="tabs-contents"),
                                ],
                            ),
                            width=6,
                        ),
                    ]
                ),
            ],
        ),
        html.Div([html.Hr(), get_busy_spot()]),
    ]
)

# Graph Callback
@app.callback(Output("Cars-per-days", "figure"), [Input("graph-update", "n_intervals")])
def update_graph(input_data):
    X = []
    Y = []

    # Data aquisition
    qty_cars = Data.quantity_cars_once_a_day()

    for keys, values in sorted(qty_cars.items()):
        print("Adding => {} : {}".format(keys, values))
        X.append(keys)
        Y.append(values)

    # Construct graph
    data = go.Scatter(x=list(X), y=list(Y), name="Scatter", mode="lines+markers")

    layout = go.Layout(
        yaxis=dict(range=[(min(Y) - 1), (max(Y) + 1)], title="Cars"),
        title=go.layout.Title(text="Quantity of cars per days"),
        xaxis=dict(rangeslider=dict(visible=True), type="date",),
    )

    return {"data": [data], "layout": layout}


# Tabs contents
@app.callback(
    Output("tabs-contents", "children"), [Input("Tabs", "value")],
)
def render_content(tab):
    if tab == "About":
        return html.Div(
            [
                html.H2(children="Smart Parking"),
                html.P(
                    "O objetivo desta aplicação é demonstrar as possibilidades que o edge-computing pode proporcionar."
                    "Com apenas uma câmera e uma DragonBoard 410c, é possivel fazer o monitoramento de um estacionamento"
                    "de forma remota, cobrindo diversas vagas."
                ),
                html.P(
                    "A DragonBoard é responsável por ler o vídeo gravado pela câmera em tempo real,"
                    "processar as imagens localmente, e enviar os dados já processados para o sistema de exibição."
                    "A economia de banda de rede do projeto é alta, já que o vídeo tem vários MB de tamanho, enquanto a informação"
                    "processada (qual vaga está ocupada e qual está livre) pesa alguns bytes."
                ),
                html.P(
                    "A visualização dos dados nessa aplicação está sendo feita na própia placa,que atua como host da aplicação Web, graças ao"
                    "poderoso processador da DragonBoard que permite rodar o algoritmo de processamento e um web-server ao mesmo tempo."
                    "Uma outra alternativa, possibilitada pela conectividade da placa, seria enviar as informações"
                    "para uma central de controle ou um serviço de nuvem."
                ),
            ],
            className="text",
        )
    elif tab == "Data":
        return html.Div(
            [
                html.Br(),
                dcc.Dropdown(
                    id="spots",
                    options=[
                        {"label": "Spot 01", "value": "0",},
                        {"label": "Spot 02", "value": "1",},
                        {"label": "Spot 03", "value": "2",},
                        {"label": "Spot 04", "value": "3",},
                        {"label": "Spot 05", "value": "4",},
                        {"label": "Spot 06", "value": "5",},
                        {"label": "Spot 07", "value": "6",},
                        {"label": "Spot 08", "value": "7",},
                        {"label": "Spot 09", "value": "8",},
                        {"label": "Spot 10", "value": "9",},
                    ],
                    placeholder="Select a Spot",
                ),
                html.Div(id="dd-container"),
            ]
        )


@app.callback(Output("dd-container", "children"), [Input("spots", "value")])
def dropdown_options(spot):
    if spot != None:
        min_time = Data.min_permanence(spot)
        mean_time = Data.mean_permanence(spot)
        max_time = Data.max_permanence(spot)
        return html.Div(
            [
                html.Div(
                    [
                        html.H2("Information"),
                        html.P("Spot number {}".format(int(spot) + 1)),
                        html.P("Min time of permanence = {}".format(min_time)),
                        html.P("Mean time of permanence = {}".format(mean_time)),
                        html.P("Max time of permanence = {}".format(max_time)),
                    ],
                    className="text",
                ),
            ]
        )


if __name__ == "__main__":
    # Start flask server
    server.run(host="0.0.0.0", port=8080, debug=True)
