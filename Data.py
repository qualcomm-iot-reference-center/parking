# -*- coding: utf-8 -*-
from MySQL_Connection import MySQL_Connection
from datetime import datetime, timedelta
import mysql.connector


class DataSmartParking:
    def __init__(self):
        self.Conn = MySQL_Connection("localhost", "db_user", "123456789", "parking")
        pass

    def Get_Data(self, arg):
        data = self.Conn.Get_Query(arg)
        self.Conn.Close_Connection()

        return data
        pass

    def Set_Data(self, arg, data):
        data = self.Conn.Set_Query(arg, data)
        self.Conn.Close_Connection()
        pass

    # Get last prediction data
    def get_prediction_data(self):
        arg = "SELECT * FROM vagas ORDER BY time_stamp DESC LIMIT 1"
        data = self.Get_Data(arg)

        return data

    # Get quantity cars once a day
    def quantity_cars_once_a_day(self):
        arg = "SELECT * FROM infos ORDER BY time_stamp ASC"
        data = self.Get_Data(arg)

        cont = 1
        data_comp = data[0][1]

        qty_cars = {}
        qty_cars[str(data_comp.date())] = 1

        for r, row in enumerate(data, 1):
            if row[1].date() == data_comp.date():
                cont += 1
                qty_cars[str(data_comp.date())] = cont
            else:
                data_comp = row[1]
                qty_cars[str(data_comp.date())] = 1
                cont = 0

        return qty_cars

    # Get quantity of cars at present
    def quantity_cars_today(self, today):

        arg = "SELECT COUNT(vaga) FROM infos WHERE time_stamp LIKE '{}%'"
        arg = arg.format(str(today.date()))
        data = self.Get_Data(arg)
        data = data[0][0]

        return data

    # Get the busiest and the less busy spot
    def busy_spot(self):
        spots = 10
        today = datetime.now().date()

        arg = (
            "SELECT SUM(permanencia) FROM infos WHERE vaga={} AND time_stamp LIKE '{}%'"
        )
        arg = arg.format(str(0), str(today))

        data = self.Get_Data(arg)
        data = data[0][0]

        busiest = data
        less_busy = data
        id_busiest = []
        id_less_busy = []

        for spot in range(1, spots):
            arg = "SELECT SUM(permanencia) FROM infos WHERE vaga={} AND time_stamp LIKE '{}%'"
            arg = arg.format(str(spot), str(today))

            data = self.Get_Data(arg)
            data = data[0][0]

            if data > busiest:
                busiest = data
                id_busiest = []
                id_busiest.append(spot)
            elif data == busiest:
                id_busiest.append(spot)

            if data < less_busy:
                less_busy = data
                id_less_busy = []
                id_less_busy.append(spot)
            elif data == less_busy:
                id_less_busy.append(spot)

        return id_busiest, id_less_busy

    # Get min time permanence
    def min_permanence(self, spot):
        today = datetime.now().date()

        arg = (
            "SELECT MIN(permanencia) FROM infos WHERE vaga={} AND time_stamp LIKE '{}%'"
        )
        arg = arg.format(str(spot), str(today))
        min = self.Get_Data(arg)
        min = min[0][0]

        return min

    # Get max time permanence
    def max_permanence(self, spot):
        today = datetime.now().date()

        arg = (
            "SELECT MAX(permanencia) FROM infos WHERE vaga={} AND time_stamp LIKE '{}%'"
        )
        arg = arg.format(str(spot), str(today))
        max = self.Get_Data(arg)
        max = max[0][0]

        return max

    # Get mean time permanence
    def mean_permanence(self, spot):
        today = datetime.now().date()

        arg = "SELECT permanencia FROM infos WHERE vaga={} AND time_stamp LIKE '{}%'"
        arg = arg.format(str(spot), str(today))
        mean = self.Get_Data(arg)

        arg = "SELECT COUNT(vaga) FROM infos WHERE vaga={} AND time_stamp LIKE '{}%'"
        arg = arg.format(str(spot), str(today))
        cars_vaga = self.Get_Data(arg)
        cars_vaga = cars_vaga[0][0]

        mean_sum = timedelta()
        for row in mean:
            mean_sum = mean_sum + row[0]

        return mean_sum / cars_vaga
