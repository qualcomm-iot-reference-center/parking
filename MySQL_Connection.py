import mysql.connector
import time


class MySQL_Connection:
    def __init__(self, host, user, password, database):
        # *** Database information ***

        self.host = host
        self.user = user
        self.password = password
        self.database = database
        pass

    def Open_connection(self):
        # *** Try open the connection with MySQL DB ***

        try:
            self.db = mysql.connector.connect(
                host=self.host,
                user=self.user,
                password=self.password,
                database=self.database,
            )
            self.cursor = self.db.cursor()
        except exception as ex:
            print("Exception:", +str(ex))
        pass

    def Get_Query(self, arg):
        # *** Get information of a table inside of a database ***

        self.Open_connection()
        self.cursor.execute(arg)
        ResultSet = self.cursor.fetchall()

        return ResultSet
        pass

    def Set_Query(self, arg, data):
        # *** Get information of a table inside of a database ***

        self.Open_connection()
        self.cursor.execute(arg, data)
        self.db.commit()
        pass

    def Close_Connection(self):
        # *** Try to close the connection with MySQL DB ***

        try:
            self.cursor.close()
            self.db.close()
        except exception as ex:
            print("Exception:", +str(ex))
        pass
