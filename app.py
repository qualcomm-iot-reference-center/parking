from flask import Flask, render_template, jsonify
from flask_mysqldb import MySQL

app = Flask(__name__)

app.config['MYSQL_USER'] = 'db_user'
app.config['MYSQL_PASSWORD'] = '123456789'
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_DB'] = 'parking'

mysql = MySQL(app)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/_update', methods=['GET'])
def update():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM vagas ORDER BY time_stamp DESC LIMIT 1')
    data=cur.fetchall()
    cur.close()
    return jsonify(vector=data[0][1:])


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=8000)