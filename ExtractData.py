# -*- coding: utf-8 -*-
from datetime import datetime, timedelta, date
from Data import DataSmartParking
import time


# initialize the variables
Data = DataSmartParking()

# Quantity of cars in the parking
spots = 10

# Get the last prediction data, that it will be used to compare with the actual data
last_data = Data.get_prediction_data()
last_data = last_data[0]

# List of times that is used to save the time of permanence of a car
date_time = list()
[date_time.append(timedelta()) for row in range(0, spots)]

while True:
    # Save prediction into DB
    # Get predict information
    with open("prediction.txt") as f:
        insert_values = f.read().split(";")

     # Argument to save into the database
    arg = "INSERT INTO vagas (v0,v1,v2,v3,v4,v5,v6,v7,v8,v9) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    Data.Set_Data(arg, insert_values)

    # Get the actual prediction data with time_stamp
    data = Data.get_prediction_data()

    # Real compare values
    # Logic: If actual_predict is 0 and last_predict is 1
    #        means that a car left a spot
    for spot in range(0, spots):
        # Set variables to facilitate the vode understanting
        time_stamp = data[0][0]
        actual_predict = data[0][(spot + 1)]
        last_predict = last_data[(spot + 1)]
        if actual_predict == 0 and last_predict == 1:
            # Esta condição foi adicionada pois caso o carro entre e saia dentro do tempo que levou para atualizar o banco de dados.
            # Para este exemplo 5 minutos. Dessa forma assumi-se que ele permaneceu ao menos o tempo de atualização(5 minutos)
            # Não precisa desta condição caso o tempo de atualização seja pequeno
            if date_time[spot] == timedelta():
                date_time[spot] = date_time[spot] + timedelta(seconds=30)

            # Insert the time of permanence of a car
            arg = "INSERT INTO infos (time_stamp,vaga,permanencia) VALUES (%s,%s,%s)"
            insert_values = (str(time_stamp), str(spot), str(date_time[spot]))
            Data.Set_Data(arg, insert_values)

            # Set spot's time to zero
            date_time[spot] = timedelta()

        # Save time if the car is still in the spot
        elif (
            actual_predict == 1
            and last_predict == 1
            and time_stamp == datetime.now().date()
        ):
            duration = datetime.combine(date.min, time_stamp.time()) - datetime.combine(
                date.min, last_data[0].time()
            )
            date_time[spot] = duration + date_time[spot]

    last_data = data[0]
    time.sleep(30)
